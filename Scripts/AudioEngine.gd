extends Node

onready var _amb = $AmbianceTree.get("parameters/playback")

func play_ambiance(target):
	if not _amb.is_playing():
		print("AnimationTree is not playing yet...")
		_amb.start("init")
		yield(get_tree().create_timer(0.5), "timeout")

	print("Transitioning to: %s" % target)
	_amb.travel(target)
