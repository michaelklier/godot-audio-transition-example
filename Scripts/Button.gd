extends Button

export(String, "a", "b") var target

# Called when the node enters the scene tree for the first time.
func _ready():
	self.connect("pressed", self, "_on_button_pressed")

func _on_button_pressed():
	print("Triggering transition to %s" % target)
	AudioEngine.play_ambiance(target)
