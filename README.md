# Godot Audio Transitions Example

This small project demo implements the idea to use Godot's AnimationTree in state machine mode
to do simple audio transitions in 2D games in a more visual way as opposed to using tweens for instance.

Is this ideal? I don't know yet. It has a couple of limitations and may seem a bit convoluted to setup,
but I think there's potential to turn this into a usable workflow.

If you have any questions or ideas feel free to get in touch via gameaudio@m-klier.de